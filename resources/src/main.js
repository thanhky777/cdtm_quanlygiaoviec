import { createApp} from 'vue';
import App from "./../src/App.vue";
import 'tailwindcss/tailwind.css';
import './../../resources/js/bootstrap';

const app = createApp(App);
app.mount('#app');